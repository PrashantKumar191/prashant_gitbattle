import React, { Component } from "react";
import { Link } from "react-router-dom";
import { TailSpin } from "react-loader-spinner";

class BattleResult extends Component {
  state = {
    data: [],
    isLoading:true
  };
  componentDidMount() {
    const user1 = this.props.match.params.playerName1;
    const user2 = this.props.match.params.playerName2;
    Promise.all([
      fetch(`https://api.github.com/users/${user1}`),
      fetch(`https://api.github.com/users/${user2}`),
    ])
      .then(([res1, res2]) => {
        return Promise.all([res1.json(), res2.json()]);
      })
      .then(([data1, data2]) => {
        this.setState({ data: [data1, data2], isLoading: false }, () => {
          this.resolveData();
        });
      });
  }
  resolveData = () => {
    const data = this.state.data;
    const score1 = data[0].public_repos;
    const score2 = data[1].public_repos;
    if (score1 < score2) {
      const temp = data[0];
      data[0] = { ...data[1], result: "Winner" };
      data[1] = { ...temp, result: "Loser" };
      this.setState({ data: data });
    } else if (score1 > score2) {
      data[0] = { ...data[0], result: "Winner" };
      data[1] = { ...data[1], result: "Loser" };
      this.setState({ data: data });
    } else if (score1 === score2) {
      data[0] = { ...data[0], result: "Tie" };
      data[1] = { ...data[1], result: "Tie" };
      this.setState({ data: data });
    }
  };

  render() {
    if (this.state.isLoading) {
      return <TailSpin type="Puff" color="#00BFFF" height={100} width={100} />;
    }
    return (
      <>
        <div className="result-container">
          <div className="result-cards">
            {this.state.data.map((card, index) => {
              return (
                <div
                  key={index}
                  className="card result-card"
                  style={{
                    backgroundColor: this.props.theme
                      ? "rgba(0, 0, 0, 0.08)"
                      : "rgba(36, 40, 42)",
                  }}
                >
                  <h1 className="card-number">{card.result}</h1>
                  <img src={card.avatar_url} alt="" />
                  <h2 className="repo-link">
                    <p>Score{card.public_repos}</p>
                    <a
                      data-links={this.props.theme ? "link-light" : "link-dark"}
                      href={card.html_url}
                    >
                      {card.login}
                    </a>
                  </h2>
                  <ul className="card-info">
                    <li>
                      <img id="icon" src="/images/resultUser_1.png" alt="" />

                      <a
                        data-links={
                          this.props.theme ? "link-light" : "link-dark"
                        }
                        className="git-profile"
                        href={card.html_url}
                      >
                        {card.name}
                      </a>
                    </li>
                    {card.location ? (
                      <li>
                        <img
                          id="icon"
                          src="/images/resultLocation_1.png"
                          alt=""
                        />{" "}
                        {card.location}
                      </li>
                    ) : (
                      ""
                    )}
                    {card.company ? (
                      <li>
                        <img src="/images/resultCompany.png" alt="" />
                        {card.company}
                      </li>
                    ) : (
                      ""
                    )}

                    <li>
                      {" "}
                      <img
                        id="icon"
                        src="/images/resultFollowers_1.png"
                        alt=""
                      />{" "}
                      {card.followers} followers
                    </li>
                    <li>
                      {" "}
                      <img
                        id="icon"
                        src="/images/resultFollowing_1.png"
                        alt=""
                      />{" "}
                      {card.following} following
                    </li>
                    <li>
                      {" "}
                      <img
                        id="icon"
                        src="/images/resultGitRepo.png"
                        alt=""
                      />{" "}
                      {card.public_repos} repositories
                    </li>
                  </ul>
                </div>
              );
            })}
          </div>
          <Link to="/battleSection" className="battle-button">
            RESET
          </Link>
        </div>
      </>
    );
  }
}

export default BattleResult;
