import React, { Component } from "react";
import {Link} from "react-router-dom"

class BattleSection extends Component {
  state = {
    value1: "",
    value2: "",
    player1: {},
    player2: {},
    player1Submit: false,
    player2Submit: false,
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const player = event.target.id;
    if (player === "player1") {
      let userInput = this.state.value1;
      this.getUser(userInput, player);
    }
    if (player === "player2") {
      let userInput = this.state.value2;
      this.getUser(userInput, player);
    }
  };
  getUser = (userName, player) => {
    fetch(`https://api.github.com/users/${userName}`)
      .then((res) => res.json())
      .then((data) => {
        if (player === "player1") {
          this.setState({ player1: data, player1Submit: true, value1: "" });
        } else {
          this.setState({ player2: data, player2Submit: true, value2: "" });
        }
      });
  };
  handleUndoForm = (e) => {
    if (e.target.id === "player1") {
      this.setState({ player1Submit: false });
    } else {
      this.setState({ player2Submit: false });
    }
  };
  handleChange = (e) => {
    let value = e.target.value;
    if (e.target.id === "player1") {
      this.setState({ value1: value });
    } else {
      this.setState({ value2: value });
    }
  };
  render() {
    const playerName1 = this.state.player1.login;
    const playerName2 = this.state.player2.login;
    return (
      <div className="battle-container">
        <div className="battle-instruction">
          <h1>Instructions</h1>
          <div className="instruction-container">
            <div className="instruction-card">
              <h2>Enter two Github users</h2>
              <img src="/images/battle-user.png" alt="" />
            </div>
            <div className="instruction-card">
              <h2>Battle</h2>
              <img src="/images/battle-jet.png" alt="" />
            </div>
            <div className="instruction-card">
              <h2>See the winner</h2>
              <img src="/images/battle-trophy.png" alt="" />
            </div>
          </div>
        </div>
        <div className="battle-footer">
          <h1>Players</h1>
          <div className="player-input">
            <div className="player">
              {this.state.player1Submit === false ? (
                <>
                  <h3>Player One</h3>
                  <form
                    id="player1"
                    className="result-form"
                    onSubmit={this.handleSubmit}
                    autocomplete="off"
                  >
                    <input
                      type="text"
                      name=""
                      id="player1"
                      placeholder="github username"
                      onChange={this.handleChange}
                      value={this.state.value1}
                    />
                    <input type="submit" value="SUBMIT" />
                  </form>
                </>
              ) : (
                <>
                  <h3>Player One</h3>
                  <div className="player-button"
                  style={{
                    backgroundColor: this.props.theme
                      ? "rgba(0, 0, 0, 0.08)"
                      : "rgba(36, 40, 42)",
                  }}>
                    <img src={this.state.player1.avatar_url} alt="" />
                    <a href={this.state.player1.html_url}>
                      {this.state.player1.login}
                    </a>
                    <button
                      id="player1"
                      className="cross-button"
                      onClick={this.handleUndoForm}
                    >
                      X
                    </button>
                  </div>
                </>
              )}
            </div>
            <div className="player">
              {this.state.player2Submit === false ? (
                <>
                  <h3>Player Two</h3>
                  <form
                    id="player2"
                    className="result-form"
                    onSubmit={this.handleSubmit}
                    autocomplete="off"
                  >
                    <input
                      type="text"
                      name=""
                      id="player2"
                      placeholder="github username"
                      onChange={this.handleChange}
                      value={this.state.value2}
                    />
                    <input type="submit" value="SUBMIT" />
                  </form>
                </>
              ) : (
                <>
                  <h3>Player Two</h3>
                  <div className="player-button" style={{
                backgroundColor: this.props.theme
                  ? "rgba(0, 0, 0, 0.08)"
                  : "rgba(36, 40, 42)",
              }}>
                    <img src={this.state.player2.avatar_url} alt="" />
                    <a href={this.state.player2.html_url}>
                      {this.state.player2.login}
                    </a>
                    <button
                      id="player2"
                      className="cross-button"
                      onClick={this.handleUndoForm}
                    >
                      X
                    </button>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
        <Link to = {`/player1=${playerName1}&player2=${playerName2}`}

          className="battle-button"
          style={{
            display:
              this.state.player1Submit && this.state.player2Submit
                ? "block"
                : "none",
              }}
        >
          BATTLE
        </Link>
      </div>
    );
  }
}

export default BattleSection;
