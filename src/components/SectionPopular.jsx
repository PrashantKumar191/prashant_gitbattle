import React, { Component } from "react";

export class SectionPopular extends Component {
  state={
    data:this.props.cards
  }
  render() {
   
    return (
      <div className="seciton-container">
        {this.props.cards.map((card, index) => {
          return (
            <div key={card.id}
              className="card"
              style={{
                backgroundColor: this.props.theme
                  ? "rgba(0, 0, 0, 0.08)"
                  : "rgba(36, 40, 42)",
              }}
            >
              <h1 className="card-number">#{index + 1}</h1>
              <img src={card.owner.avatar_url} alt="" />
              <h2 className="repo-link">
                <a
                  data-links={this.props.theme ? "link-light" : "link-dark"}
                  href={card.html_url}
                >
                  {card.owner.login}
                </a>
              </h2>
              <ul className="card-info">
                <li>
                  <img id="icon" src="/images/login.png" alt="" />
                  
                  <a
                    data-links={this.props.theme ? "link-light" : "link-dark"}
                    className="git-profile"
                    href={card.owner.html_url}
                  >
                    {card.owner.login}
                  </a>
                </li>
                <li>
                  <img id="icon" src="/images/star.png" alt="" />
                  {card.watchers} stars
                </li>
                <li>
                  <img id="icon" src="/images/fork.png" alt="" />
                  {card.forks} forks
                </li>
                <li>
                  <img src="/images/issues.png" alt="" />
                  {card.open_issues} open issues
                </li>
              </ul>
            </div>
          );
        })}
      </div>
    );
  }
}

export default SectionPopular;
