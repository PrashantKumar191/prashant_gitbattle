import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export class Header extends Component {
  render() {
    return (
      <nav id="header">
        <ul className="ulitem">
          <li>
            <NavLink
              data-links={this.props.theme ? "link-light" : "link-dark"}
              to="/"
              exact={true}
            >
              Popular
            </NavLink>
          </li>
          <li>
            <NavLink
              data-links={this.props.theme ? "link-light" : "link-dark"}
              to="/battleSection"
            >
              Battle
            </NavLink>
          </li>
        </ul>
        <button className="switch-icon" onClick={this.props.onThemeChange}>
          {this.props.theme ? "🔦" : "💡"}
        </button>
      </nav>
    );
  }
}

export default Header;
