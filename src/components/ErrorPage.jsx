import React, { Component } from 'react'

export default class ErrorPage extends Component {
  render() {
    return (
      <div className='error'>
          <img src="/images/ErrorMessage.png" alt="page not found" />
      </div>
    )
  }
}
