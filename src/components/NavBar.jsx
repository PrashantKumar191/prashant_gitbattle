import React, { Component } from "react";
import SectionPopular from "./SectionPopular";
import { TailSpin } from "react-loader-spinner";

const languages = ["All", "JavaScript", "Ruby", "Java", "CSS", "Python"];

export class NavBar extends Component {
  state = {
    val: [],
    isLoading:true
  };
  componentDidMount = () => {
    fetch(
      "https://api.github.com/search/repositories?q=stars:%3E1+language:All&sort=stars&order=desc&type=Repositories"
    )
      .then((res) => res.json())
      .then((data) => {
        this.setState({ val: data.items ,isLoading:false});
      });
  };
  handleRender = (language) => {
    this.setState({isLoading:true });
    fetch(
      `https://api.github.com/search/repositories?q=stars:%3E1+language:${language}&sort=stars&order=desc&type=Repositories`
    )
      .then((res) => res.json())
      .then((data) => {
        this.setState({ val: data.items,isLoading:false });
      });
  };

  render() {
    if (this.state.isLoading) {
      return <TailSpin type="Puff" color="#00BFFF" height={100} width={100} />;
    }

    return (
      <>
        <div className="nav-bar">
          {languages.map((language) => {
            return (
              <div key={language}>
                <input type="radio" name="language" id={language} />
                <label
                  className="switch-language"
                  onClick={() => this.handleRender(language)}
                  htmlFor={language}
                >
                  {language}
                </label>
              </div>
            );
          })}
        </div>
        <SectionPopular cards={this.state.val} theme={this.props.theme} />
      </>
    );
  }
}

export default NavBar;
