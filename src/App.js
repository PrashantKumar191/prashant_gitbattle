import "./App.css";
import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import NavBar from "./components/NavBar";
import BattleSection from "./components/BattleSection";
import BattleResult from "./components/BattleResult";
import ErrorPage from "./components/ErrorPage";
class App extends Component {
  state = {
    theme: true,
  };

  handleThemeChange = () => {
    this.setState({ theme: !this.state.theme });
  };

  render() {
    return (
      <div className="App">
        <div className={this.state.theme ? "light" : "dark"}>
          <div className="container">
            <Header
              onThemeChange={this.handleThemeChange}
              theme={this.state.theme}
            />

            <Switch>
              <Route
                exact
                path="/"
                render={(props) => <NavBar theme={this.state.theme} />}
              />
              <Route
                exact
                path="/battleSection"
                render={(props) => <BattleSection theme={this.state.theme} />}
              />
              <Route
                exact
                path={`/player1=:playerName1&player2=:playerName2`}
                render={(props) => (
                  <BattleResult theme={this.state.theme} {...props} />
                )}
              />
              <Route exact path="*" component={ErrorPage}/>
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
